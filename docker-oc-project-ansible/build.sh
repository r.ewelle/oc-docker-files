#!/bin/bash

case "$1" in
        initial)
            docker build -t=ansible-base ./ansible/framework/ansible-base
            docker build -t=ansible-node ./ansible/framework/ansible-node
            docker build -t=ansible-manager ./ansible/framework/ansible-manager


            ;;

        ansible-base)
            docker build -t=ansible-base ./ansible/framework/ansible-base
            ;;

        ansible-manager)
            docker build -t=ansible-manager ./ansible/framework/ansible-manager
            ;;

        ansible-node)
            docker build -t=ansible-node ./ansible/framework/ansible-node
            ;;

        *)
            echo $"Usage: $0 {initial|ansible-manager|ansible-node}"
            exit 1
esac
