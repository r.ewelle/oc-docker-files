#!/bin/bash

# change node root password
echo 'root:manager123456' | chpasswd

# add user-ansible and create password
adduser user-ansible && \
echo 'user-ansible:ansible123456' | chpasswd && \
usermod -aG wheel user-ansible

# setting the virtual env
virtualenv ansible2.7.10
source ansible2.7.10/bin/activate
pip3 install -r requirements.txt
