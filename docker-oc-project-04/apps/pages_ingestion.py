#! /usr/bin/env python3
import sys
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from datetime import datetime

import time


def main(argv):

    starting_time = time.time()
    dt_string = datetime.now().strftime("%d-%m-%Y_%H%M")
#    host_ip = "localhost"
    # host_ip = "172.18.0.3"
    host_ip = "namenode"

    spark = SparkSession.builder.getOrCreate()

    revision_type = StructType([
        StructField("id", LongType(), True),
        StructField("contributor",
                    StructType([
                        StructField("id", LongType(), True),
                        StructField("ip", StringType(), True),
                        StructField("username", StringType(), True)]))])

    schema = StructType([
        StructField("id", StringType(), True),
        StructField("title",  StringType(), True),
        StructField("revision", ArrayType(revision_type, True), True)])

    pages_df = spark.read.format('xml')\
        .options(rowTag='page', mode='FAILFAST')\
        .schema(schema) \
        .load("hdfs://"+host_ip+":9000/data/raw/pages/frwiki-20200101-stub-meta-history.xml")\
        .repartition(16)\
        .persist()

    print("Number of partitions: {}".format(pages_df.rdd.getNumPartitions()))

    pages_df.show()
    pages_df.printSchema()

    #  Saves the dataframe of the Avro records
    filename = "hdfs://"+host_ip+":9000/data/master/pages/"+dt_string
    pages_df.write.format("avro").save(filename)

    execution_time = (time.time() - starting_time)
    print("	Execution time: ", execution_time)

    # Hang script to tune it with Spark Web UI
    # (available @ http://localhost:4040)
    input("press ctrl+c to exit")


if __name__ == "__main__":
    main(sys.argv[1:])
