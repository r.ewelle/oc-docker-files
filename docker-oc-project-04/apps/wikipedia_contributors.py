#! /usr/bin/env python3
import sys
from pyspark import SparkContext
from pyspark.sql.functions import col, explode
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession
from graphframes import *
from datetime import datetime
import time


def getcontributor(revision):
    return revision.contributor.username if revision.contributor.username else revision.contributor.ip


def main(argv):

    starting_time = time.time()
    dt_string = datetime.now().strftime("%d-%m-%Y_%H%M")
    # host_ip = "172.17.0.1"
    host_ip = "namenode"
    to_find = ["Anthropologie marxiste"]

    sc = SparkContext.getOrCreate()
    spark = SparkSession \
        .builder \
        .appName("wikipedia_contriutors") \
        .getOrCreate()
    sqlContext = SQLContext(sc)

    # Read pages and pagelinks avro data
    vertices = spark.read.format("avro").load(
        "hdfs://"+host_ip+":9000/data/master/pages/08-05-2020_0241")
    edges = spark.read.format("avro").load(
        "hdfs://"+host_ip+":9000/data/master/pagelinks/08-05-2020_0350")

    gf = GraphFrame(vertices, edges)
    gf.vertices.repartition(16)\
               .persist()
    gf.edges.repartition(16)\
            .persist()

    print("gf vertices number of partitions: {}".format(gf.vertices.rdd.getNumPartitions()))
    print("gf edges number of partitions: {}".format(gf.edges.rdd.getNumPartitions()))

    # showing graph data
    gf.vertices.show()
    gf.edges.show()

    # createing a subgraph
    to_find_df = gf.vertices.filter(gf.vertices.title.isin(to_find))
    to_find_df.show()
    keys = to_find_df.rdd.map(lambda p: p.id).collect()

    filtered_gf = gf.filterEdges(gf.edges.src.isin(
        keys) | gf.edges.dst.isin(keys)).dropIsolatedVertices()
    filtered_gf.vertices.persist()
    filtered_gf.edges.persist()
    gf.vertices.unpersist()
    gf.edges.unpersist()

    filtered_gf.vertices.show()
    print("filtered_gf.vertices.count(): ", filtered_gf.vertices.count())
    filtered_gf.edges.show()
    print("filtered_gf.edges.count(): ", filtered_gf.edges.count())

    exploded_df = filtered_gf.vertices.withColumn('revision', explode('revision'))
    exploded_df.show()
    exploded_df.printSchema()
    print("exploded_df.count(): ", exploded_df.count())

    exploded_rdd = exploded_df.rdd.map(lambda p: (p.id, p.title, getcontributor(p.revision)))
    reduced_df = exploded_rdd.toDF(['id', 'title', 'contributor']).persist()
    reduced_df.show()
    reduced_df.printSchema()

    contrib_df = reduced_df.groupBy("contributor").count().sort(col("count").desc()).limit(3)
    contrib_df.show()

    #  Saves the dataframe of the json records
    filename = "hdfs://"+host_ip+":9000/data/contributors/" + \
        to_find[0].lower().replace(' ', '-')+"_"+dt_string
    contrib_df.write.format("json").save(filename)

    execution_time = (time.time() - starting_time)
    print("	Execution time: ", execution_time)

    # Hang script to tune it with Spark Web UI
    # (available @ http://localhost:4040)
    # input("press ctrl+c to exit")


if __name__ == "__main__":
    main(sys.argv[1:])
