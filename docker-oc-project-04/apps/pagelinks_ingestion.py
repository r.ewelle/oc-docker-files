#! /usr/bin/env python3
import sys
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from datetime import datetime
import time


def main(argv):

    starting_time = time.time()
    dt_string = datetime.now().strftime("%d-%m-%Y_%H%M")
    # host_ip = "172.17.0.1"
    # host_ip = "localhost"
    host_ip = "namenode"

    sc = SparkContext.getOrCreate()
    session = SparkSession \
        .builder \
        .appName("pagelinks_ingestion") \
        .getOrCreate()

    # Read pagelinks sql data
    pagelinks_rdd = sc.textFile(
        "hdfs://"+host_ip+":9000/data/raw/pagelinks/frwiki-20200101-pagelinks.sql")

    # parse sql file, extract the records and transform to new RDD
    pagelinks_rdd = pagelinks_rdd.filter(lambda x: "INSERT INTO" in x) \
                                 .map(lambda x: x.rstrip(';')) \
                                 .map(lambda x: x.lstrip("INSERT INTO `pagelinks` VALUES ")) \
                                 .map(lambda x: x[1:-1]) \
                                 .flatMap(lambda x: x.split('),(')) \
                                 .map(lambda x: (x.split(',')[0].rstrip(','), x.split(',')[2].rstrip(',')))

    pagelinks_df = pagelinks_rdd.toDF(['pl_from', 'pl_title'])\
        .repartition(16)\
        .persist()

    print("pagelinks_df number of partitions: {}".format(pagelinks_df.rdd.getNumPartitions()))
    print("partitioner: {}".format(pagelinks_df.rdd.partitioner))

    pagelinks_df.show()

    # Read pages sql data
    pages_rdd = sc.textFile(
        "hdfs://"+host_ip+":9000/data/raw/pagelinks/frwiki-20200101-page.sql")

    print("pages_rdd number of partitions: {}".format(pages_rdd.getNumPartitions()))
    print("Partitioner: {}".format(pages_rdd.partitioner))

    # parse sql file, extract the records and transform to new RDD
    pages_rdd = pages_rdd.filter(lambda x: "INSERT INTO" in x) \
        .map(lambda x: x.rstrip(';')) \
        .map(lambda x: x.lstrip("INSERT INTO `page` VALUES ")) \
        .map(lambda x: x[1:-1]) \
        .flatMap(lambda x: x.split('),(')) \
        .map(lambda x: (x.split(',')[0], x.split(',')[2]))

    pages_df = pages_rdd.toDF(['id', 'title'])\
        .repartition(16)\
        .persist()

    print("pages_df number of partitions: {}".format(pages_df.rdd.getNumPartitions()))
    print("partitioner: {}".format(pages_df.rdd.partitioner))

    pages_df.show()

    cond = [pages_df.title == pagelinks_df.pl_title]
    pagelinks_df = pagelinks_df.join(pages_df, cond, 'inner').drop(
        pagelinks_df.pl_title).drop(pages_df.title)

    pagelinks_df = pagelinks_df.selectExpr("pl_from as src", "id as dst")
    pagelinks_df.show()

    pages_df.unpersist()

    #  Saves the dataframe of the Avro records
    filename = "hdfs://"+host_ip+":9000/data/master/pagelinks/"+dt_string
    pagelinks_df.write.format("avro").save(filename)

    execution_time = (time.time() - starting_time)
    print("	Execution time: ", execution_time)

    # Hang script to tune it with Spark Web UI
    # (available @ http://localhost:4040)
    input("press ctrl+c to exit")


if __name__ == "__main__":
    main(sys.argv[1:])
