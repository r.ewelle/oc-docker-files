#! /usr/bin/env python3
import sys
import time
import pymongo
import getopt
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession
from pyspark.sql import Window
from datetime import datetime, timedelta
from itertools import chain
from pyspark.sql.functions import col, explode, row_number


def main(argv):

    starting_time = time.time()
    host_ip = "namenode"
    replicatset_credentials = "root:password123@mongodb-primary:27017,mongodb-secondary:27017"

    conf = SparkConf().setAppName("compute_trending_hastags")
    conf.set("spark.mongodb.input.uri", "mongodb://" + replicatset_credentials +
             "/twitterdb.view.trendinghashtags?replicaSet=replicaset&authSource=admin&readPreference=primaryPreferred")
    conf.set("spark.mongodb.output.uri", "mongodb://" + replicatset_credentials +
             "/twitterdb.view.trendinghashtags?replicaSet=replicaset&authSource=admin")

    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    spark = SparkSession.builder.getOrCreate()

    # Parameters parsing
    datestring = ""
    try:
        opts, args = getopt.getopt(argv, "hd:", ["date="])
    except getopt.GetoptError:
        print("usage: spark-submit --master spark://spark-master:7077 compute_trending_hastags.py -d <date>")
        print("example: spark-submit --master spark://spark-master:7077 compute_trending_hastags.py -d 20200531")

        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("usage: spark-submit --master spark://spark-master:7077 compute_trending_hastags.py -d <date>")
            print("example: spark-submit --master spark://spark-master:7077 compute_trending_hastags.py -d 20200531")
            sys.exit()
        elif opt in ("-d", "--date"):
            datestring = arg

    # use previous day if date not provided
    if datestring == "":
        datestring = (datetime.now() - timedelta(days=1)).strftime('%Y%m%d')
        # Read hashtags avro data into dataframes
    hashtag_df = spark.read.format("avro").load(
        "hdfs://"+host_ip+":9000/data/master/hashtags-"+datestring+"*")\
        .persist()

    print("Number of partitions: {}".format(hashtag_df.rdd.getNumPartitions()))
    print("Number of tuples: {}".format(hashtag_df.count()))

    exploded_df = hashtag_df.withColumn('hashtag', explode('hashtags'))
    tendances_df = exploded_df.groupBy("time", "hashtag").count().sort(
        col("time").asc(), col("count").desc())

    window = Window.partitionBy(tendances_df['time']).orderBy(tendances_df['count'].desc())
    bestof_df = tendances_df.select(
        '*', row_number().over(window).alias('row_number')).filter(col('row_number') <= 10)

    mongo_client = pymongo.MongoClient(
        "mongodb://" + replicatset_credentials + "/?replicaSet=replicaset")
    database = mongo_client["twitterdb"]

    times = chain(*sorted(bestof_df.select("time").distinct().collect()))
    for time_string in times:
        time_df = bestof_df.where(col("time").eqNullSafe(time_string))
        trending_map = time_df.select("hashtag", "count").rdd.collect()
        time_df.show()

        # using timestamps as document id
        date = datetime.strptime(time_string, "%Y-%m-%dT%H:%M:%SZ")
        timestamp = int(datetime.timestamp(date)) * 1000
        trending_rdd = sc.parallelize([(str(timestamp), date,  trending_map)])
        trending_df = sqlContext.createDataFrame(trending_rdd, ["_id", "time", "trending"])

        # write timely trendings to mongodb
        trending_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").save()

        # check if the data is saved then clear out obselete element in speed layer
        document = database["view.trendinghashtags"].find_one({"_id": str(timestamp)})
        if document:
            print(" deleted document: " + str(timestamp))
            database["speed.trendinghashtags"].delete_one({"_id": str(timestamp)})

    execution_time = (time.time() - starting_time)
    print("	Execution time: ", execution_time)

    # Hang script to tune it with Spark Web UI
    # (available @ http://localhost:4040)
    #input("press ctrl+c to exit")


if __name__ == "__main__":
    main(sys.argv[1:])
