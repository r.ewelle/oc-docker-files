#!/bin/bash

mkdir -p $SPARK_SUBMIT_LOG
ln -sf /dev/stdout $SPARK_SUBMIT_LOG/spark-submit.out

cd /opt/notebooks
jupyter notebook --port=9999 --ip=0.0.0.0 --allow-root --debug >> $SPARK_SUBMIT_LOG/spark-submit.out
