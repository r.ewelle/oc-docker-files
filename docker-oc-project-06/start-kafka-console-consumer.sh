#!/bin/bash

ZK_HOST=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' zookeeper)
# Only works for container ports, that are mapped/exposed on the Host
ZK_PORT=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "2181/tcp") 0).HostPort }}' zookeeper)

docker run -it --rm \
        --name=kafka-console-consumer \
        --network dockerocproject05_default\
        ches/kafka /bin/bash -c "/kafka/bin/kafka-console-consumer.sh --zookeeper $ZK_HOST:$ZK_PORT --topic $1"
