version: "3.3"
services:
    zookeeper:
      image: zookeeper
      container_name: zookeeper
      restart: always
      ports:
        - "2181:2181"

    kafka_1:
      image: kafka
      container_name: kafka_1
      depends_on:
        - zookeeper
      links:
        - zookeeper:zk
      ports:
        - "9092:9092"
      environment:
        KAFKA_BROKER_ID: "101"
        KAFKA_ADVERTISED_HOST_NAME: kafka_1
        ZOOKEEPER_IP: zookeeper
        KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
        KAFKA_JMX_HOSTNAME: "kafka_1"
        KAFKA_JMX_PORT: 9093
        KAFKA_JMX_OPTS: "-Djava.rmi.server.hostname=kafka_1
        -Dcom.sun.management.jmxremote.local.only=false
        -Dcom.sun.management.jmxremote.rmi.port=9093
        -Dcom.sun.management.jmxremote.port=9093
        -Dcom.sun.management.jmxremote.authenticate=false
        -Dcom.sun.management.jmxremote.ssl=false"


    kafka_2:
      image: kafka
      container_name: kafka_2
      depends_on:
        - zookeeper
      links:
        - zookeeper:zk
      ports:
        - "9093:9092"
      environment:
        KAFKA_BROKER_ID: "102"
        KAFKA_ADVERTISED_HOST_NAME: kafka_2
        ZOOKEEPER_IP: zookeeper
        KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
        KAFKA_JMX_HOSTNAME: "kafka_2"
        KAFKA_JMX_PORT: 9093
        KAFKA_JMX_OPTS: "-Djava.rmi.server.hostname=kafka_2
        -Dcom.sun.management.jmxremote.local.only=false
        -Dcom.sun.management.jmxremote.rmi.port=9093
        -Dcom.sun.management.jmxremote.port=9093
        -Dcom.sun.management.jmxremote.authenticate=false
        -Dcom.sun.management.jmxremote.ssl=false"

    kafka-manager:
      image: sheepkiller/kafka-manager:latest
      container_name: kafka-manager
      ports:
        - "9001:9000"
      links:
        - zookeeper
        - kafka_1
        - kafka_2
      depends_on:
        - zookeeper
        - kafka_2
        - kafka_1
      environment:
        ZK_HOSTS: zookeeper:2181
        APPLICATION_SECRET: letmein
        KM_ARGS: -Djava.net.preferIPv4Stack=true

    schema-registry:
      image: confluentinc/cp-schema-registry:5.5.0
      hostname: schema-registry
      container_name: schema-registry
      depends_on:
        - zookeeper
        - kafka_2
        - kafka_1
      ports:
        - "8081:8081"
        - "9021:9021"
      environment:
        SCHEMA_REGISTRY_HOST_NAME: schema-registry
        SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL: 'zookeeper:2181'

    nimbus:
      image: storm-nimbus
      container_name: nimbus
      restart: always
      ports:
        - "3772:3772"
        - "3773:3773"
        - "6627:6627"
      depends_on:
        - zookeeper
      links:
        - zookeeper:zk

    supervisor:
      image: storm-supervisor
      container_name: supervisor
      restart: always
      ports:
        - "8000"
      depends_on:
        - zookeeper
        - nimbus
      links:
        - zookeeper:zk
        - nimbus:nimbus

    stormui:
      image: storm-ui
      container_name: storm-ui
      restart: always
      ports:
        - "8080:8080"
      depends_on:
        - zookeeper
        - nimbus
      links:
        - zookeeper:zk
        - nimbus:nimbus

    spark-master:
        image: spark-master:latest
        container_name: spark-master
        ports:
          - "9090:8080"
          - "7077:7077"
        volumes:
           - ./apps:/opt/spark-apps
           - ./data:/opt/spark-data
        environment:
          - "SPARK_LOCAL_IP=spark-master"

    spark-worker:
        image: spark-worker:latest
        depends_on:
          - spark-master
        environment:
          - SPARK_MASTER=spark://spark-master:7077
          - SPARK_WORKER_CORES=4
          - SPARK_WORKER_MEMORY=8G
          - SPARK_DRIVER_MEMORY=2G
          - SPARK_EXECUTOR_MEMORY=2G
        volumes:
           - ./apps:/opt/spark-apps
           - ./data:/opt/spark-data

    spark-submit:
        image: spark-submit:latest
        container_name: spark-submit
        depends_on:
          - spark-master
        ports:
          - "9999:9999"
          - "5000:5000"
          - "4040:4040"
        environment:
          - SPARK_MASTER=spark://spark-master:7077
        volumes:
           - ./apps:/opt/spark-apps
           - ./data:/opt/spark-data
           - /home/ewelle/notebooks:/opt/notebooks
        environment:
          - "SPARK_LOCAL_IP=spark-submit"

    namenode:
        image: bde2020/hadoop-namenode:1.1.0-hadoop2.8-java8
        container_name: namenode
        volumes:
          - /data/docker-hdfs/namenode:/hadoop/dfs/name
          - /home/ewelle/oc-data:/opt/oc-data
        environment:
          - CLUSTER_NAME=test
        env_file:
          - ./hadoop.env
        ports:
          - 50070:50070
          - 9000:9000

    datanode_1:
        image: bde2020/hadoop-datanode:1.1.0-hadoop2.8-java8
        container_name: datanode_1
        depends_on:
          - namenode
        volumes:
          - /data/docker-hdfs/datanode_1:/hadoop/dfs/data
        env_file:
          - ./hadoop.env
        ports:
          - 50075:50075

    datanode_2:
        image: bde2020/hadoop-datanode:1.1.0-hadoop2.8-java8
        container_name: datanode_2
        depends_on:
          - namenode
        volumes:
          - /data/docker-hdfs/datanode_2:/hadoop/dfs/data
        env_file:
          - ./hadoop.env
        ports:
          - 50085:50075

    mongodb-primary:
        image: 'docker.io/bitnami/mongodb:4.2-debian-10'
        container_name: mongodb-primary
        ports:
          - 27017:27017
        environment:
          - MONGODB_ADVERTISED_HOSTNAME=mongodb-primary
          - MONGODB_REPLICA_SET_MODE=primary
          - MONGODB_ROOT_PASSWORD=password123
          - MONGODB_REPLICA_SET_KEY=replicasetkey123
        volumes:
          - /data/docker-mongodb/primary:/bitnami/mongodb

    mongodb-secondary:
        image: 'docker.io/bitnami/mongodb:4.2-debian-10'
        container_name: mongodb-secondary
        ports:
          - 27018:27017
        depends_on:
          - mongodb-primary
        environment:
          - MONGODB_ADVERTISED_HOSTNAME=mongodb-secondary
          - MONGODB_REPLICA_SET_MODE=secondary
          - MONGODB_PRIMARY_HOST=mongodb-primary
          - MONGODB_PRIMARY_ROOT_PASSWORD=password123
          - MONGODB_REPLICA_SET_KEY=replicasetkey123
        volumes:
          - /data/docker-mongodb/secondary:/bitnami/mongodb

    mongodb-arbiter:
        image: 'docker.io/bitnami/mongodb:4.2-debian-10'
        container_name: mongodb-arbiter
        depends_on:
          - mongodb-primary
        environment:
          - MONGODB_ADVERTISED_HOSTNAME=mongodb-arbiter
          - MONGODB_REPLICA_SET_MODE=arbiter
          - MONGODB_PRIMARY_HOST=mongodb-primary
          - MONGODB_PRIMARY_ROOT_PASSWORD=password123
          - MONGODB_REPLICA_SET_KEY=replicasetkey123
        volumes:
          - /data/docker-mongodb/arbiter:/bitnami/mongodb
