#!/bin/bash

case "$1" in
        initial)
            docker build -t=storm-base ./storm/framework/storm-base
            docker build -t=storm ./storm/framework/storm

            docker build -t=kafka-producer ./kafka/kafka-producer
            docker build -t=storm-topology ./storm/storm-topology

            docker build -t=spark-base ./spark/spark-base
            docker build -t=spark-master ./spark/spark-master
            docker build -t=spark-worker ./spark/spark-worker
            docker build -t=spark-submit ./spark/spark-submit

            docker build -t=zookeeper ./zookeeper/framework
            docker build -t=kafka ./kafka/framework
            docker build -t=storm-nimbus ./storm/framework/storm-nimbus
            docker build -t=storm-supervisor ./storm/framework/storm-supervisor
            docker build -t=storm-ui ./storm/framework/storm-ui

            ;;

        spark)
            docker build -t=spark-base ./spark/spark-base
            docker build -t=spark-submit ./spark/spark-submit
            ;;

        spark-submit)
            docker build -t=spark-submit ./spark/spark-submit
            ;;

        kafka-producer)
            docker build -t=kafka-producer ./kafka/kafka-producer
            ;;

        storm-topology)
            docker build -t=storm-topology ./storm/storm-topology
            ;;

        *)
            echo $"Usage: $0 {initial|kafka-producer|storm-topology|spark-submit}"
            exit 1
esac
