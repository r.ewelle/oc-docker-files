package twitter;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.hdfs.avro.AvroUtils;
import org.apache.storm.hdfs.bolt.AvroGenericRecordBolt;
import org.apache.storm.hdfs.bolt.format.SimpleFileNameFormat;
import org.apache.storm.hdfs.bolt.rotation.FileRotationPolicy;
import org.apache.storm.hdfs.bolt.rotation.TimedRotationPolicy;
import org.apache.storm.hdfs.bolt.sync.CountSyncPolicy;
import org.apache.storm.hdfs.bolt.sync.SyncPolicy;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;

public class HashtagTopology
{
	private static final Logger logger = LoggerFactory.getLogger(HashtagTopology.class);
	private String bootstrapServers;

	public HashtagTopology(String BOOTSTRAP_SERVERS) {
		bootstrapServers = BOOTSTRAP_SERVERS;
	}

	/**
	 * TwitterTopology with Kafka
	 *
	 * @return      StormTopology Object
	 */
	public StormTopology buildTopology(String TOPIC, String MONGODB_SERVERS, String SCHEMA_REGISTRY_URL, String HDFS_URL) {

		TopologyBuilder builder = new TopologyBuilder();
		KafkaSpoutConfig.Builder spoutConfigBuilder = new KafkaSpoutConfig.Builder(bootstrapServers, StringDeserializer.class, KafkaAvroDeserializer.class, TOPIC);
		spoutConfigBuilder.setProp(SCHEMA_REGISTRY_URL_CONFIG, "http://" + SCHEMA_REGISTRY_URL);
		spoutConfigBuilder.setProp(AUTO_OFFSET_RESET_CONFIG, "earliest");
		spoutConfigBuilder.setGroupId("hashtag-trends");
		KafkaSpoutConfig spoutConfig = spoutConfigBuilder.build();


		builder.setSpout("kafka-spout", new KafkaSpout(spoutConfig));

		// From kafka to hdfs
		builder.setBolt("avro-parsing", new HashtagAvroParsingBolt())
				.shuffleGrouping("kafka-spout");

		builder.setBolt("hdfs-bolt", buildHdfsBolt(HDFS_URL))
				.shuffleGrouping("avro-parsing");


		// From kafka to mongoDB
		builder.setBolt("hashtag-splitter", new HashtagSplitterBolt())
				.shuffleGrouping("kafka-spout");

		builder.setBolt("hashtag-counter", new HashtagCounterBolt().withTumblingWindow(BaseWindowedBolt.Duration.of(1000*60)), 2)
				.fieldsGrouping("hashtag-splitter", new Fields("hashtag"));

		builder.setBolt("trending-hashtag", new TrendingHashtagBolt())
				.globalGrouping("hashtag-counter");

		builder.setBolt("save-trending-hashtags",  new SaveTrendingHashtagBolt(MONGODB_SERVERS))
				.shuffleGrouping("trending-hashtag");

		return builder.createTopology();
	}

	public AvroGenericRecordBolt buildHdfsBolt(String HDFS_URL) {

		// sync the filesystem after every 1k tuples
		SyncPolicy syncPolicy = new CountSyncPolicy(100);
		// Specify the user name of Hadoop If not specified, it may throw an exception without permission when HDFS creates the directory (RemoteException: Permission denied)
		System.setProperty("HADOOP_USER_NAME", "hdfs");
		// rotate files every hour
		FileRotationPolicy rotationPolicy = new TimedRotationPolicy(1.0f, TimedRotationPolicy.TimeUnit.HOURS);

		SimpleFileNameFormat fileNameFormat = new SimpleFileNameFormat()
				.withName("hashtags-$TIME.avro")
				.withPath("/data/master/")
				.withTimeFormat("yyyyMMdd-HHmm");

		AvroGenericRecordBolt bolt = new AvroGenericRecordBolt()
				.withFsUrl("hdfs://"+ HDFS_URL)
				.withFileNameFormat(fileNameFormat)
				.withRotationPolicy(rotationPolicy)
				.withSyncPolicy(syncPolicy);

		return bolt;
	}
	public static void main( String[] args ) throws AlreadyAliveException, InvalidTopologyException, AuthorizationException, ClassNotFoundException {

		Config conf = new Config();
		String TOPOLOGY_NAME;

		if (args != null && args.length > 0) {

			TopologyBuilder builder = new TopologyBuilder();

			TOPOLOGY_NAME = args[0];
			String ZK_HOST = args[1];
			int ZK_PORT = Integer.parseInt(args[2]);
			String BOOTSTRAP_SERVERS = args[3];
			String NIMBUS_HOST = args[4];
			int NIMBUS_THRIFT_PORT = Integer.parseInt(args[5]);
			String MONGODB_SERVERS = args[6];
			String TOPIC = args[7];
			String SCHEMA_REGISTRY_URL = args[8];
			String HDFS_URL = args[9];


			conf.setNumWorkers(2);
			conf.setMessageTimeoutSecs(60 * 30);
			conf.put(Config.NIMBUS_HOST, NIMBUS_HOST);
			conf.put(Config.NIMBUS_THRIFT_PORT, NIMBUS_THRIFT_PORT);
			conf.put(Config.STORM_ZOOKEEPER_PORT, ZK_PORT);
			conf.put(Config.STORM_ZOOKEEPER_SERVERS, Arrays.asList(ZK_HOST));

			AvroUtils.addAvroKryoSerializations(conf);
			HashtagTopology hashtagTopology = new HashtagTopology(BOOTSTRAP_SERVERS);
			StormSubmitter.submitTopology(TOPOLOGY_NAME, conf, hashtagTopology.buildTopology(TOPIC, MONGODB_SERVERS, SCHEMA_REGISTRY_URL, HDFS_URL));
		} else {
			logger.error("Please submit topology with required arguments! ");
		}
	}
}
