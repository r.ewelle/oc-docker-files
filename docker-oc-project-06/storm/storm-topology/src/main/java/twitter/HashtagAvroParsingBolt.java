package twitter;

import org.apache.avro.generic.GenericRecord;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class HashtagAvroParsingBolt extends BaseRichBolt {
	private static final Logger logger = LoggerFactory.getLogger(HashtagAvroParsingBolt.class);
	private OutputCollector outputCollector;

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		outputCollector = collector;
	}

	@Override
	public void execute(Tuple input) {
		try {
			process(input);
		} catch (Exception e) {
			e.printStackTrace();
			outputCollector.fail(input);
		}
	}

	public void process(Tuple tuple){
		GenericRecord result = (GenericRecord) tuple.getValueByField("value");
		logger.info("Processing, time: " + result.get("time") + " hashtags: " + result.get("hashtags")+" result: "+result.toString());

		outputCollector.emit(new Values(result));
		outputCollector.ack(tuple);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("record"));
	}
}
