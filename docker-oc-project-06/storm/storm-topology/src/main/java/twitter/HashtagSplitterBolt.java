package twitter;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class HashtagSplitterBolt extends BaseRichBolt {
	private static final Logger logger = LoggerFactory.getLogger(HashtagSplitterBolt.class);
	private OutputCollector outputCollector;

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		outputCollector = collector;
	}

	@Override
	public void execute(Tuple input) {
		try {
			process(input);
		} catch (Exception e) {
			e.printStackTrace();
			outputCollector.fail(input);
		}
	}

	public void process(Tuple tuple) {
		GenericRecord result = (GenericRecord) tuple.getValueByField("value");
		String time = result.get("time").toString();
		List<Utf8> hashtags = (List<Utf8>) result.get("hashtags");
		logger.info("Processing, time: " + time + " hashtags: " + hashtags);
		for (Utf8 hashtag : hashtags) {
			outputCollector.emit(new Values(time, hashtag.toString()));
		}
		outputCollector.ack(tuple);
	}


	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("time","hashtag"));
	}
}
