#! /usr/bin/env python

from datetime import datetime
import uuid
import twitter  # pip install twitter
import pytz
import sys

from confluent_kafka.avro import AvroProducer
from argparse import ArgumentParser
from confluent_kafka import avro


def load_avro_schema_from_file(schema_file):
    key_schema_string = """
    {"type": "string"}
    """

    key_schema = avro.loads(key_schema_string)
    value_schema = avro.load(schema_file)

    return key_schema, value_schema


def parse_command_line_args():
    arg_parser = ArgumentParser()

    arg_parser.add_argument("--topic", required=True, help="Topic name")
    arg_parser.add_argument("--bootstrap-servers", required=False,
                            default="localhost:9092", help="Bootstrap server address")
    arg_parser.add_argument("--schema-registry", required=False,
                            default="http://localhost:8081", help="Schema Registry url")
    arg_parser.add_argument("--token", required=False,
                            default="290802860-Sfb2x3SC6EXKgWSC6agyChBnJV9ahyANRWSnneHC", help="Twitter token")
    arg_parser.add_argument("--token_secret", required=False,
                            default="yHo5AZYy51TeBQDWu06Gi6tm4a0rUPkJcYhXV6PaFpyUj", help="Twitter token_secret")
    arg_parser.add_argument("--consumer_key", required=False,
                            default="wcF8mA1jasWd5ZB8O5UkmmtVx", help="Twitter consumer_key")
    arg_parser.add_argument("--consumer_secret", required=False,
                            default="jhibEFYbTQ2cOhd2hyDMI0Ts1fVMYiqgkTSOEnFYBmhxWjIi0h", help="Twitter consumer_secret")

    return arg_parser.parse_args()


def main(args):

    if args.topic is None:
        raise AttributeError("--topic is not provided.")

    key_schema, value_schema = load_avro_schema_from_file("tweet.avsc")

    producer_config = {
        "bootstrap.servers": args.bootstrap_servers,
        "schema.registry.url": args.schema_registry,
        "queue.buffering.max.messages": 10000000,
        "queue.buffering.max.ms": 5000
    }

    producer = AvroProducer(producer_config, default_key_schema=key_schema,
                            default_value_schema=value_schema)

    oauth = twitter.OAuth(args.token, args.token_secret, args.consumer_key,
                          args.consumer_secret)
    t = twitter.TwitterStream(auth=oauth)

    # Get id_str as id, text as text, user.screen_name as by,
    sample_tweets = t.statuses.sample()
    for tweet in sample_tweets:
        if "delete" in tweet:
            # Deleted tweet events do not have any associated text
            continue

        # Collect hashtags
        if "entities" in tweet and "hashtags" in tweet["entities"]:
            hashtags = [h['text'] for h in tweet["entities"]["hashtags"]]
            if len(hashtags) > 0:

                # Get the date and hour from the timestamp
                time_string = datetime.fromtimestamp(
                    int(tweet["timestamp_ms"])/1000).replace(tzinfo=pytz.utc).strftime('%Y-%m-%dT%H:00:00Z')

                # time_string = datetime.fromtimestamp(
                #     int(tweet["timestamp_ms"])/1000).replace(tzinfo=pytz.utc).strftime('%Y-%m-%dT%H:%M:00Z')

                date = datetime.fromtimestamp(
                    int(tweet["timestamp_ms"])/1000).replace(tzinfo=pytz.utc).strftime('%Y%m%d')

                document = {
                    "time": time_string,
                    "hashtags": [h['text'] for h in tweet["entities"]["hashtags"]]
                }

                # Send to kafka broker
                try:
                    producer.produce(topic=args.topic, key=str(uuid.uuid4()), value=document)
                    print(document)
                    producer.poll(0)
                except BufferError as e:
                    print(e, file=sys.stderr)
                    producer.poll(1)

    producer.flush()


if __name__ == "__main__":
    main(parse_command_line_args())
