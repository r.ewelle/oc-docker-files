#!/bin/bash

export PRODUCER_LOG=/logs
mkdir -p $PRODUCER_LOG

#ln -sf /dev/stdout $PRODUCER_LOG/hashtags.log

# python collect_hashtags.py --topic $TOPIC  --bootstrap-servers $KAFKA_BROKERS --schema-registry $SCHEMA_REGISTRY \
# --token $TOKEN --token_secret $TOKEN_SECRET --consumer_key $CONSUMER_KEY --consumer_secret $CONSUMER_SECRET >> $PRODUCER_LOG/hashtags.log

python collect_hashtags.py --topic $TOPIC  --bootstrap-servers $KAFKA_BROKERS --schema-registry $SCHEMA_REGISTRY \
--token $TOKEN --token_secret $TOKEN_SECRET --consumer_key $CONSUMER_KEY --consumer_secret $CONSUMER_SECRET
