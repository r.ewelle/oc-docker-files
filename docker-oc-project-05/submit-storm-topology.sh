#!/bin/bash


ZK_HOST=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' zookeeper)
KFK_HOST_1=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' kafka_1)
KFK_HOST_2=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' kafka_2)
NIMBUS_HOST=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' nimbus)
MG_HOST_PRIMARY=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mongodb-primary)
MG_HOST_SECONDARY=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mongodb-secondary)

# Only works for container ports, that are mapped/exposed on the Host
ZK_PORT=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "2181/tcp") 0).HostPort }}' zookeeper)
KFK_PORT_1=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "9092/tcp") 0).HostPort }}' kafka_1)
KFK_PORT_2=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "9092/tcp") 0).HostPort }}' kafka_2)
NIMBUS_THRIFT_PORT=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "6627/tcp") 0).HostPort }}' nimbus)
MG_PORT_PRIMARY=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "27017/tcp") 0).HostPort }}' mongodb-primary)
MG_PORT_SECONDARY=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "27017/tcp") 0).HostPort }}' mongodb-secondary)

BOOTSTRAP_SERVERS=${KFK_HOST_1}:${KFK_PORT_1},${KFK_HOST_2}:${KFK_PORT_2}
MONGODB_SERVERS=${MG_HOST_PRIMARY}:${MG_PORT_PRIMARY},${MG_HOST_SECONDARY}:${MG_PORT_SECONDARY}

docker run -it --rm \
        -e MAINCLASS=$1 \
        -e TOPOLOGY_NAME=$2 \
        -e ZK_HOST=${ZK_HOST} \
        -e ZK_PORT=${ZK_PORT} \
        -e BOOTSTRAP_SERVERS=${BOOTSTRAP_SERVERS} \
        -e NIMBUS_HOST=${NIMBUS_HOST} \
        -e NIMBUS_THRIFT_PORT=${NIMBUS_THRIFT_PORT} \
        -e MONGODB_SERVERS=${MONGODB_SERVERS} \
        -e TOPIC=$3 \
        --network dockerocproject05_default\
        --name topology \
        storm-topology \
        "submit"
