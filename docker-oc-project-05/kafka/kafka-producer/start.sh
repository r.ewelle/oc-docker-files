#!/bin/bash
# python collect_hashtags.py $KAFKA_BROKERS $HDFS_HOST $HDFS_PORT $TOPIC $TOKEN $TOKEN_SECRET $CONSUMER_KEY $CONSUMER_SECRET

python collect_hashtags.py --topic $TOPIC  --bootstrap-servers $KAFKA_BROKERS --schema-registry $SCHEMA_REGISTRY \
--token $TOKEN --token_secret $TOKEN_SECRET --consumer_key $CONSUMER_KEY --consumer_secret $CONSUMER_SECRET
