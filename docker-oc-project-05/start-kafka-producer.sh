#!/bin/bash

BROKER_HOST_1=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' kafka_1)
BROKER_HOST_2=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' kafka_2)
SCHEMA_REGISTRY_HOST=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' schema-registry)
# Only works for container ports, that are mapped/exposed on the Host
BROKER_PORT_1=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "9092/tcp") 0).HostPort }}' kafka_1)
BROKER_PORT_2=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "9092/tcp") 0).HostPort }}' kafka_2)
SCHEMA_REGISTRY_PORT=$(docker inspect --format '{{ (index (index .NetworkSettings.Ports "8081/tcp") 0).HostPort }}' schema-registry)

KAFKA_BROKERS=${BROKER_HOST_1}:${BROKER_PORT_1},${BROKER_HOST_2}:${BROKER_PORT_2}
SCHEMA_REGISTRY=http://${SCHEMA_REGISTRY_HOST}:${SCHEMA_REGISTRY_PORT}

docker run -it --rm \
        -e KAFKA_BROKERS=$KAFKA_BROKERS \
        -e SCHEMA_REGISTRY=$SCHEMA_REGISTRY \
        -e TOPIC=$1 \
        --env-file kafka-producer.env \
        --network dockerocproject05_default\
        --name producer \
        kafka-producer
