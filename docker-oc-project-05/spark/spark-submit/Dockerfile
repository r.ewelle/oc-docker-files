FROM spark-base:latest

COPY spark-submit.sh /
COPY requirements.txt /

ENV SPARK_MASTER_URL="spark://spark-master:7077"
ENV SPARK_SUBMIT_LOG /spark/logs
ENV SPARK_SUBMIT_ARGS=""
ENV SPARK_APPLICATION_ARGS ""

RUN export SPARK_HOME=/spark
RUN export PATH=$PATH:$SPARK_HOME/bin

# Setup python dependencies
RUN python -m pip install --upgrade pip
RUN python -m pip install jupyter ipyparallel pandas networkx py2cytoscape visJS2jupyter
RUN ipcluster nbextension enable
RUN pip install -r requirements.txt


# Download Spark Packages
RUN wget http://dl.bintray.com/spark-packages/maven/graphframes/graphframes/0.7.0-spark2.4-s_2.11/graphframes-0.7.0-spark2.4-s_2.11.jar -qO $SPARK_HOME/jars/graphframes.jar
RUN wget https://repo1.maven.org/maven2/org/apache/spark/spark-avro_2.11/2.4.3/spark-avro_2.11-2.4.3.jar -qO $SPARK_HOME/jars/spark-avro.jar
RUN wget https://repo1.maven.org/maven2/com/databricks/spark-csv_2.11/1.5.0/spark-csv_2.11-1.5.0.jar -qO $SPARK_HOME/jars/spark-csv_2.11-1.5.0.jar
RUN wget https://repo1.maven.org/maven2/com/databricks/spark-xml_2.11/0.6.0/spark-xml_2.11-0.6.0.jar -qO $SPARK_HOME/jars/spark-xml_2.11-0.6.0.jar
RUN wget https://repo1.maven.org/maven2/org/apache/commons/commons-csv/1.1/commons-csv-1.1.jar -qO $SPARK_HOME/jars/commons-csv-1.1.jar
RUN wget https://repo1.maven.org/maven2/com/univocity/univocity-parsers/1.5.1/univocity-parsers-1.5.1.jar -qO $SPARK_HOME/jars/univocity-parsers-1.5.1.jar
RUN wget https://repo1.maven.org/maven2/commons-io/commons-io/2.6/commons-io-2.6.jar -qO $SPARK_HOME/jars/commons-io-2.6.jar

RUN wget https://repo1.maven.org/maven2/org/mongodb/spark/mongo-spark-connector_2.11/2.4.1/mongo-spark-connector_2.11-2.4.1.jar -qO $SPARK_HOME/jars/mongo-spark-connector_2.11-2.4.1.jar
RUN wget https://repo1.maven.org/maven2/org/mongodb/mongo-java-driver/3.10.2/mongo-java-driver-3.10.2.jar -qO $SPARK_HOME/jars/mongo-java-driver-3.10.2.jar

CMD ["pyspark", "--packages", "graphframes:graphframes:0.7.0-spark2.4-s_2.11"]
CMD ["pyspark", "--packages", "org.apache.spark:spark-avro_2.11:2.4.3"]
CMD ["pyspark", "--packages", "com.databricks:spark-csv_2.11:1.5.0,com.databricks:spark-xml_2.11:0.6.0"]
CMD ["pyspark", "--packages", "org.mongodb.spark:mongo-spark-connector_2.11:2.4.1"]


CMD ["/bin/bash", "/spark-submit.sh"]
