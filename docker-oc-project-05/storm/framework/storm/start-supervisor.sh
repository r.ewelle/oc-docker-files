ZK_HOST=$(getent hosts zookeeper | awk '{ print $1 }')
NIMBUS_HOST=$(getent hosts nimbus | awk '{ print $1 }')
sed -i -e "s/%zookeeper%/$ZK_HOST/g" $STORM_HOME/conf/storm.yaml
sed -i -e "s/%nimbus%/$NIMBUS_HOST/g" $STORM_HOME/conf/storm.yaml

echo "storm.local.hostname: `hostname -i`" >> $STORM_HOME/conf/storm.yaml

/usr/sbin/sshd && supervisord
