package twitter;


import com.mongodb.*;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SaveTrendingHashtagBolt extends BaseRichBolt {
	private static final Logger logger = LoggerFactory.getLogger(SaveTrendingHashtagBolt.class);
	private OutputCollector outputCollector;
	private static final String DB = "twitterdb";
	private static final String COLLECTION = "speed.trendinghashtags";
	private static final String DBUSER = "root";
	private static final String DBPASS = "password123";
	private static final String ADMINDB = "admin";
	private MongoClient mongoClient;
	private List<ServerAddress> mongoServers;


	public SaveTrendingHashtagBolt(String MONGODB_SERVERS) {
		mongoServers = new ArrayList<>();
		for(String server : MONGODB_SERVERS.split("\\s*,\\s*")){
			logger.info("Setting mongodb server address: "+server);
			String [] items = server.split("\\s*:\\s*");
			mongoServers.add(new ServerAddress(items[0], Integer.parseInt(items[1])));
		}
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		outputCollector = collector;
		MongoCredential mongoCredential = MongoCredential.createScramSha1Credential(DBUSER, ADMINDB,
				DBPASS.toCharArray());
		mongoClient = new MongoClient(mongoServers, Arrays.asList(mongoCredential));
	}

	@Override
	public void execute(Tuple input) {
		try {
			process(input);
			outputCollector.ack(input);
		} catch (Exception e) {
			e.printStackTrace();
			outputCollector.fail(input);
		}
	}
	
	public void process(Tuple input) throws IOException, ParseException {
		String time = input.getStringByField("time");
		List<List> trending = (List) input.getValueByField("trending");

		logger.info("Recieved trending : " + trending);

		List<Object> trendingList = new ArrayList<>();
		LinkedHashMap<String,Object> hashtagMap;
		for (List list : trending) {
			hashtagMap = new LinkedHashMap<>();
			hashtagMap.put("hashtag", (String) list.get(0));
			hashtagMap.put("count", (Long) list.get(1));
			trendingList.add(hashtagMap);
		}

		DateFormat normalDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SS'Z'");
		Date date = normalDf.parse(time);

		DB database = mongoClient.getDB(DB);
		DBCollection collection = database.getCollection(COLLECTION);
		BasicDBObject trends = new BasicDBObject();
		trends.put("_id", Long.toString(date.getTime()));
		trends.put("time", date);
		trends.put("trending", trendingList);
		logger.info("Saving hashtag trends to mongodb: "+trends);
		collection.insert(trends);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}
}
