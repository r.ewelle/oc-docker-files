package twitter;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TrendingHashtagBolt extends BaseRichBolt {
	private static final Logger logger = LoggerFactory.getLogger(TrendingHashtagBolt.class);
	private OutputCollector outputCollector;
	private List<List> trending;
	private static final Integer TOPN = 10;
	private String currentDate;



	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		outputCollector = collector;
		trending = new ArrayList<List>();
		currentDate = "";
	}

	@Override
	public void execute(Tuple input) {
		String time = input.getStringByField("time");
		// Check if hour has changed
		if (!currentDate.equals(time) && !trending.isEmpty()) {
			logger.info("Emitting, trending: " + trending);
			outputCollector.emit(new Values(currentDate, new ArrayList(trending)));
			outputCollector.ack(input);
			currentDate = time;
			trending = new ArrayList<List>();
		}
			rankHashtag(input);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("time","trending"));
	}

	private void rankHashtag(Tuple tuple) {
		String hashtag = tuple.getStringByField("hashtag");
		long count = tuple.getLongByField("count");
		Integer existingIndex = find(hashtag);
		if (null != existingIndex) {
			long existingValue = (long) trending.get(existingIndex).get(1);
			List<Object> values = new ArrayList<>();
			values.add(hashtag);
			values.add(existingValue + count );
			trending.set(existingIndex, values);

		}else {
			List<Object> values = new ArrayList<>();
			values.add(hashtag);
			values.add(count);
			trending.add(values);
		}

		Collections.sort(trending, new Comparator<List>() {
			@Override
			public int compare(List o1, List o2) {
				return compareRanking(o1, o2);
			}
		});

		shrinkRanking();
	}

	private Integer find(String hashtag) {
		for(int i = 0; i < trending.size(); ++i) {
			String current = (String) trending.get(i).get(0);
			if (current.equals(hashtag)) {
				return i;
			}
		}
		return null;
	}

	private int compareRanking(List one, List two) {
		long valueOne = (Long) one.get(1);
		long valueTwo = (Long) two.get(1);
		long delta = valueTwo - valueOne;
		if(delta > 0) {
			return 1;
		} else if (delta < 0) {
			return -1;
		} else {
			return 0;
		}
	}

	private void shrinkRanking() {
		int size = trending.size();
		if (TOPN >= size) return;
		for (int i = TOPN; i < size; i++) {
			trending.remove(trending.size() - 1);
		}
	}
}