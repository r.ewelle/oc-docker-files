package twitter;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class HashtagCounterBolt extends BaseWindowedBolt {
	private static final Logger logger = LoggerFactory.getLogger(HashtagCounterBolt.class);
	private OutputCollector outputCollector;
	private Map<String, Long> hashtagCount;



	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		outputCollector = collector;
	}

	@Override
	public void execute(TupleWindow tupleWindow) {
		int tupleCount = 0;
		hashtagCount = new HashMap<String, Long>();
		String time = "";

		for(Tuple input : tupleWindow.get()) {
			time = input.getStringByField("time");
			String hashtag = input.getStringByField("hashtag");
			Long count = hashtagCount.get(hashtag);
			if (count == null) {
				count = 0L;
			}

			count++;
			hashtagCount.put(hashtag, count);
			outputCollector.ack(input);
		}

		for (Map.Entry entry : hashtagCount.entrySet()) {
			logger.info("Emitting, time: " + time + "hashtag: " + entry.getKey() + ",	 count: " + entry.getValue());
			outputCollector.emit(new Values(time, entry.getKey(), entry.getValue()));
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("time", "hashtag", "count"));
	}
}