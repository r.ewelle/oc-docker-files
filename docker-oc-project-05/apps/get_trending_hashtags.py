#!/usr/bin/env python3

import pymongo
import sys
import getopt
import pprint

from datetime import datetime, timedelta


def get_previous_hour():
    timestring = datetime.now().strftime('%Y-%m-%dT%H')
    date_formated = datetime.strptime(timestring, '%Y-%m-%dT%H')
    return (date_formated - timedelta(hours=1))


def get_previous_day():
    datestring = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%dT23')
    end = datetime.strptime(datestring, '%Y-%m-%dT%H')
    return (end - timedelta(days=1)), end


def get_day(daystring):
    end = datetime.strptime(daystring, '%Y-%m-%dT%H')
    return (end - timedelta(days=1)), end


def get_previous_24hours():
    timestring = datetime.now().strftime('%Y-%m-%dT%H')
    date_formated = datetime.strptime(timestring, '%Y-%m-%dT%H')
    end = date_formated - timedelta(hours=1)
    return (end - timedelta(days=1)), end


def get_24hours(timestring):
    end = datetime.strptime(timestring, '%Y-%m-%dT%H')
    return (end - timedelta(days=1)), end


def main(argv):

    # connection string
    replicatset_credentials = "root:password123@mongodb-primary:27017,mongodb-secondary:27017/?replicaSet=replicaset"

    # Parameters parsing
    period = 'hour'
    value = ''
    try:
        opts, args = getopt.getopt(
            argv, "hp:v:",
            ["period=", "value="])

    except getopt.GetoptError:
        print("usage: python get_trendinghashtags.py -p <period> -v <value>")
        print("example: python get_trendinghashtags.py -p day -v 2020-05-21")

        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("usage: python get_trending_hashtags.py -p <period> -v <value>")
            print("example: python get_trending_hashtags.py -p day -v 2020-05-21")
            sys.exit()

        elif opt in ("-p", "--period"):
            period = arg
        elif opt in ("-v", "--value"):
            value = arg

    print("\n")
    print("	-----------------------------------------------------------------"
          "---------------------------")
    print("	-                              GET TRENDING HASHTAGS "
          "                                      -")
    print("	------------------------------------------------------------------"
          "--------------------------")

    print("")
    print("	Parameters:")
    print("		period: ", period)
    print("		value : ", value)
    print("")

    documents = []
    query = {}
    mongo_client = pymongo.MongoClient("mongodb://" + replicatset_credentials)
    database = mongo_client["twitterdb"]

    # setting up query for hourly requests
    if period == "hour":
        if value == "":
            value = get_previous_hour()
        else:
            value = datetime.strptime(value, '%Y-%m-%dT%H')
        query = {"time": value}
    else:
        # setting up query for daily requests
        if period == "day":
            if value == "":
                start, end = get_previous_day()
            else:
                start, end = get_day(value + "T23")
            query = {"time": {"$gt": start, "$lte": end}}
        else:
            # setting up query for 24hours requests
            if period == "24hours":
                if value == "":
                    start, end = get_previous_24hours()
                else:
                    start, end = get_24hours(value)
                query = {"time": {"$gt": start, "$lte": end}}

    print("	query: " + str(query))
    print("")
    # Querying for trending hashtags chronologically  ordered
    for document in database["view.trendinghashtags"].find(query):
        documents.append(document)

    for document in database["speed.trendinghashtags"].find(query):
        documents.append(document)

    print("	documents: ")
    print("")
    pprint.pprint(documents)


if __name__ == "__main__":
    main(sys.argv[1:])
