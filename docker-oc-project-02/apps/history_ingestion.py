#! /usr/bin/env python3
import sys
import xml.etree.ElementTree as ET
from pyspark.sql import SparkSession
from pyspark.sql import Row
from datetime import datetime


def parse_xml(rdd):
    """
    Read the xml string from rdd, parse and extract the elements,
    then return a list of list.
    """
    tree = ET.fromstring(rdd[0])
    pages = []
    for page in tree.findall('page'):
        page_id = int(page.find('id').text)
        ns = int(page.find('ns').text)
        title = page.find('title').text

        revisions = []
        for revision in page.findall('revision'):
            parentid = ''
            revision_id = int(revision.find('id').text)
            if revision.find('parentid') is not None:
                parentid = int(revision.find('parentid').text)

            timestamp = revision.find('timestamp').text
            contributorjson = {}
            for contributor in revision.findall('contributor'):
                contributor_id = ''
                ip = ''
                username = ''

                if contributor.find('id') is not None:
                    contributor_id = int(contributor.find('id').text)

                if contributor.find('ip') is not None:
                    ip = contributor.find('ip').text

                if contributor.find('username') is not None:
                    username = contributor.find('username').text

                contributorjson = {
                    "id": contributor_id,
                    "ip": ip,
                    "username": username
                }

            revisions.append({
                "id": revision_id,
                "parentid": parentid,
                "timestamp": timestamp,
                "contributor": contributorjson
            })

        pages.append({
            "id": page_id,
            "namespace": ns,
            "title": title,
            "revisions": revisions
        })

    return pages


def main(argv):

    # with SparkSession.builder \
    #         .master("local[2]") \
    #         .config("spark.sql.shuffle.partitions", 16) \
    #         .getOrCreate() as spark:

    with SparkSession.builder \
            .getOrCreate() as spark:

        # Read xml data
        file_rdd = spark.read.text(
            "hdfs://localhost:9000/data/raw/history/*.xml", wholetext=True)\
            .rdd
        dt_string = datetime.now().strftime("%d-%m-%Y_%H%M")

        print("Before flatMap")
        print("Number of partitions: {}".format(file_rdd.getNumPartitions()))
        print("Partitioner: {}".format(file_rdd.partitioner))

        # parse xml tree, extract the records and transform to new RDD
        pages_rdd = file_rdd.flatMap(parse_xml)

        # Convert to a resilient distributed dataset (RDD) of rows
        pages_rows = pages_rdd.map(lambda page: Row(**page))  # .partitionBy(2)
        pages_df = spark.createDataFrame(pages_rows)

        # df = spark.createDataFrame(pages_rows)
        # # Repartition by column
        # pages_df = df.repartition("id")
        # print("\nNumber of partitions: {}".format(pages_df.rdd.getNumPartitions()))

        pages_df.persist()
        pages_df.show()
        pages_df.count()

        #  Saves the dataframe of the Avro records
        filename = "hdfs://localhost:9000/data/master/history/"+dt_string
        # pages_df.write.format("avro").save(filename)

        # Hang script to tune it with Spark Web UI
        # (available @ http://localhost:4040)
        input("press ctrl+c to exit")


if __name__ == "__main__":
    main(sys.argv[1:])
