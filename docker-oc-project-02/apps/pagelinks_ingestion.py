#! /usr/bin/env python3
import sys
from pyspark.context import SparkContext
from pyspark.sql import SparkSession
from datetime import datetime
import time


def main(argv):

    starting_time = time.time()
    dt_string = datetime.now().strftime("%d-%m-%Y_%H%M")
    # host_ip = "172.17.0.1"
    host_ip = "172.18.0.1"
    # host_ip = "ewelle-G3-3590"

    sc = SparkContext.getOrCreate()
    session = SparkSession \
        .builder \
        .appName("pagelinks_ingestion") \
        .config("spark.hadoop.dfs.client.use.datanode.hostname", "true") \
        .config("spark.hadoop.dfs.datanode.use.datanode.hostname", "true") \
        .getOrCreate()

    # Read sql data
    pagelinks_rdd = sc.textFile(
        "hdfs://"+host_ip+":9000/data/raw/pagelinks/frwiki-20200101-pagelinks-01.sql")

    print("Number of partitions: {}".format(pagelinks_rdd.getNumPartitions()))
    print("Partitioner: {}".format(pagelinks_rdd.partitioner))

    # parse sql file, extract the records and transform to new RDD
    pagelinks_rdd = pagelinks_rdd.filter(lambda x: "INSERT INTO" in x) \
                                 .map(lambda x: x.rstrip(';')) \
                                 .map(lambda x: x.lstrip("INSERT INTO `pagelinks` VALUES ")) \
                                 .map(lambda x: x[1:-1]) \
                                 .flatMap(lambda x: x.split('),(')) \
                                 .map(lambda x: (x.split(',')[0].rstrip(','), x.split(',')[1].rstrip(','), x.split(',')[2].rstrip(','), x.split(',')[3].rstrip(',')))

    pagelinks_df = pagelinks_rdd.toDF(['pl_from', 'pl_namespace', 'pl_title', 'pl_from_namespace'])
    pagelinks_df.show()
    # pagelinks_df.count()

    #  Saves the dataframe of the Avro records
    filename = "hdfs://"+host_ip+":9000/data/master/pagelinks/"+dt_string
    pagelinks_df.write.format("avro").save(filename)

    execution_time = (time.time() - starting_time)
    print("	Execution time: ", execution_time)

    # Hang script to tune it with Spark Web UI
    # (available @ http://localhost:4040)
    input("press ctrl+c to exit")


if __name__ == "__main__":
    main(sys.argv[1:])
