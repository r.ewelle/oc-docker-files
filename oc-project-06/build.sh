#!/bin/bash

case "$1" in
        initial)
            docker build -t=spark-submit ./spark/spark-submit
            docker build -t=kafka-producer ./kafka/kafka-producer
            docker build -t=storm-topology ./storm/storm-topology
            ;;

        spark-submit)
            docker build -t=spark-submit ./spark/spark-submit
            ;;

        kafka-producer)
            docker build -t=kafka-producer ./kafka/kafka-producer
            ;;

        storm-topology)
            docker build -t=storm-topology ./storm/storm-topology
            ;;

        *)
            echo $"Usage: $0 {initial|kafka-producer|storm-topology|spark-submit}"
            exit 1
esac
