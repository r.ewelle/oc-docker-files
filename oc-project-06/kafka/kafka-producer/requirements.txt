twitter==1.18.0
pytz==2020.1
avro-python3==1.9.0
confluent-kafka[avro]==1.4.0
