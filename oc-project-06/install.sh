#!/bin/bash

# install helm charts
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo list
helm install oc-mongodb bitnami/mongodb --set serviceType=NodePort,architecture=replicaset,auth.rootPassword=password123


# Confluent kafka
helm repo add confluentinc https://confluentinc.github.io/cp-helm-charts/
helm repo update
helm install confluentinc confluentinc/cp-helm-charts -f kubernetes/confluent-values.yaml

# spark
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo list
helm install spark bitnami/spark


# hdfs
helm repo add gradiant https://gradiant.github.io/charts/
helm install hdfs gradiant/hdfs


#storm
cd ./kubernetes/
helm install oc storm/


# install applications
cd ../
eval $(minikube docker-env)
./build.sh initial

cd kubernetes/
kubectl apply -f producer-config-map.yaml
kubectl apply -f storm-config-map.yaml

kubectl apply -f producer-manifest.yml
kubectl apply -f storm-manifest.yml
kubectl apply -f spark-submit-manifest.yml
