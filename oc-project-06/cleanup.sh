#!/bin/bash

minikube stop
minikube delete
minikube start --vm-driver=virtualbox --disk-size 35g --memory 16g --cpus 6
