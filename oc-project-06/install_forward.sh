#!/bin/bash

#hdfs dataset
kubectl exec -it hdfs-namenode-0 -- bash
hdfs dfs -mkdir /user
hdfs dfs -mkdir /user/root
hdfs dfs -mkdir /data
hdfs dfs -mkdir /data/master
hdfs dfs -chown root:hdfs /user/root
hdfs dfs -chown root:hdfs /data/master

# port forwarding
kubectl port-forward service/oc-mongodb-headless 27017 27017
kubectl port-forward service/oc-storm-ui 8080 8080
kubectl port-forward --namespace default spark-master-0 8181:8080
